import React, { Fragment } from "react";

import RenderGame from "./components/Game/RenderGame";
import Footer from "./components/Layout/Footer";

function App() {
  return (
    <Fragment>
      <RenderGame />
      <Footer />
    </Fragment>
  );
}

export default App;
