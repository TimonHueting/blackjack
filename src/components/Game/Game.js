import React, { useState, useEffect, Fragment } from "react";

import classes from "./Game.module.css";
import Modal from "../UI/Modal";
import Button from "../UI/Button";
import BackSideCard from "../../assets/backside-card.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";

const Game = (props) => {
  const [playerCards, setPlayerCards] = useState([]);
  const [dealerCards, setDealerCards] = useState([]);
  const [cardsDealt, setCardsDealt] = useState(false);
  const [playerPoints, setPlayerPoints] = useState(0);
  const [dealerPoints, setDealerPoints] = useState(0);
  const [dealersTurn, setDealersTurn] = useState(false);
  const [gameEnded, setGameEnded] = useState(false);
  const [resultMsg, setResultMsg] = useState("");
  const closeIcon = <FontAwesomeIcon icon={faXmark} size="lg" />;

  useEffect(() => {
    const dealInitialCards = async () => {
      await fetch(
        `https://deckofcardsapi.com/api/deck/${props.deck_id}/draw/?count=4`,
        {
          headers: {
            Accept: "application/json",
          },
        }
      )
        .then((res) => res.json())
        .then((res) => {
          setPlayerCards([res.cards[0], res.cards[2]]);
          setDealerCards([res.cards[1], res.cards[3]]);
          setCardsDealt(true);
        });
    };

    dealInitialCards();
  }, [props]);

  const countPoints = (type, cards) => {
    let currentCount = 0;
    for (let index = 0; index < cards.length; ++index) {
      if (cards[index].value.trim().length > 1) {
        if (cards[index].value === "ACE") {
          if (currentCount + 11 > 21) {
            currentCount = currentCount + 1;
          } else {
            currentCount = currentCount + 11;
          }
        } else {
          currentCount = currentCount + 10;
        }
      } else {
        currentCount = currentCount + parseInt(cards[index].value);
      }
    }

    if (type === "player") {
      if (currentCount > 21) {
        setTimeout(() => {
          setGameEnded(true);
          setResultMsg("You're bust! You lost.");
          setDealersTurn(true);
        }, 1000);
      }

      if (currentCount === 21) {
        setDealersTurn(true);
      }
    }

    if (type === "dealer") {
      if (dealersTurn) {
        if (gameEnded === false) {
          if (currentCount === 21) {
            setTimeout(() => {
              setGameEnded(true);
            }, 1500);
            setResultMsg("The dealer has BlackJack! You lost.");
          }

          if (currentCount >= 17) {
            setTimeout(() => {
              setGameEnded(true);
            }, 1500);

            if (currentCount === playerPoints) {
              setResultMsg("It's a tie. Therefore, the dealer wins.");
            }

            if (currentCount > 21) {
              setResultMsg("The dealer is bust. You win!");
            }

            if (currentCount > playerPoints && currentCount < 21) {
              setResultMsg("The dealer has higher cards. You lost.");
            }

            if (currentCount < playerPoints && currentCount < 21) {
              setResultMsg("You out played the dealer. You win!");
            }
          } else {
            setTimeout(() => {
              drawCard("dealer");
            }, 1000);
          }
        }
      }
    }

    return currentCount;
  };

  useEffect(() => {
    const currentPlayerCount = countPoints("player", playerCards);
    setPlayerPoints(currentPlayerCount);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [playerCards]);

  useEffect(() => {
    const currentDealerCount = countPoints("dealer", dealerCards);
    setDealerPoints(currentDealerCount);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dealerCards, dealersTurn]);

  const drawCard = async (type) => {
    await fetch(
      `https://deckofcardsapi.com/api/deck/${props.deck_id}/draw/?count=1`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((res) => res.json())
      .then((res) => {
        if (type === "player") {
          setPlayerCards([...playerCards, res.cards[0]]);
        }

        if (type === "dealer") {
          setDealerCards([...dealerCards, res.cards[0]]);
        }
      });
  };

  return (
    <Fragment>
      <div className={classes.game__table}>
        <h2 className={classes.game__points}>Your hand: {playerPoints}</h2>
        <div className={classes.dealer__side}>
          <ul>
            {cardsDealt &&
              (dealersTurn ? (
                dealerCards.map((card) => {
                  return (
                    <li key={Math.random()}>
                      <img src={card.image} alt={card.value}></img>
                    </li>
                  );
                })
              ) : (
                <Fragment>
                  <li key={Math.random()}>
                    <img
                      src={dealerCards[0].image}
                      alt={dealerCards[0].value}
                    ></img>
                  </li>
                  <li key={Math.random()}>
                    <img
                      className={classes.backside}
                      src={BackSideCard}
                      alt="Backside of a BlackJack card"
                    ></img>
                  </li>
                </Fragment>
              ))}
          </ul>
        </div>
        <div className={classes.player__side}>
          <ul>
            {cardsDealt &&
              playerCards.map((card) => {
                return (
                  <li key={Math.random()}>
                    <img src={card.image} alt={card.value} />
                  </li>
                );
              })}
          </ul>
        </div>
        {!dealersTurn && (
          <div className={classes.game__controls}>
            <Button onClick={() => drawCard("player")}>Hit</Button>
            <Button
              onClick={() => {
                setDealersTurn(true);
              }}
            >
              Stand
            </Button>
          </div>
        )}
      </div>

      {gameEnded && (
        <Modal>
          <div className={classes.result}>
            <h1 className={classes.result__title}>{resultMsg}</h1>
            <div className={classes.result__results}>
              Your hand: {playerPoints} <br />
              Dealers hand: {dealerPoints}
            </div>
            <div
              onClick={() => window.location.reload(false)}
              className={classes.result__close}
            >
              {closeIcon}
            </div>
            <div
              className={classes["button__play-again"]}
              onClick={() => setGameEnded(false) + setDealersTurn(false)}
            >
              <Button onClick={props.onChangeState}>Play again</Button>
            </div>
          </div>
        </Modal>
      )}
    </Fragment>
  );
};

export default Game;
