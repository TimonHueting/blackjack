import React, { Fragment, useEffect, useState } from "react";

import Game from "./Game";
import classes from "./RenderGame.module.css";
import Button from "../UI/Button";
import GameRules from "./GameRules";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleInfo } from "@fortawesome/free-solid-svg-icons";

const RenderGame = () => {
  const [deck, setDeck] = useState();
  const [gameStarted, setGameStarted] = useState(false);
  const [showGameRules, setShowGameRules] = useState(false);
  const infoIcon = <FontAwesomeIcon icon={faCircleInfo} size="lg" />;

  useEffect(() => {
    const getDeck = async () => {
      await fetch(
        "https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=6",
        {
          headers: {
            Accept: "application/json",
          },
        }
      )
        .then((res) => res.json())
        .then((res) => {
          setDeck(res);
        });
    };
    getDeck();
  }, []);

  return (
    <Fragment>
      {!gameStarted ? (
        <Fragment>
          {showGameRules ? (
            <GameRules
              onHideRules={() => {
                setShowGameRules(false);
              }}
            />
          ) : (
            <div
              className={classes.game__rules}
              onClick={() => setShowGameRules(true)}
            >
              {infoIcon}
            </div>
          )}
          <div className={classes.game__intro}>
            <h1 className={classes.game__title}>BlackJack</h1>
            <Button onClick={() => setGameStarted(true)}>Play</Button>
          </div>
        </Fragment>
      ) : (
        <Game onChangeState={setGameStarted} deck_id={deck.deck_id} />
      )}
    </Fragment>
  );
};

export default RenderGame;
