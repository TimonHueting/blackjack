import classes from "./GameRules.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";

const GameRules = (props) => {
  const closeIcon = <FontAwesomeIcon icon={faXmark} size="lg" />;

  return (
    <div className={classes["game-rules"]}>
      <div onClick={props.onHideRules} className={classes["game-rules__close"]}>
        {closeIcon}
      </div>
      <h1 className={classes["game-rules__title"]}>BlackJack Rules</h1>
      <p className={classes["game-rules__introduction"]}>
        The goal of blackjack is to beat the dealer's hand without going over
        21. Within this game six-decks are shuffled together to play with. When
        the game begins the dealer gives two cards to the player and two to him
        self, one of which will be placed faced down.
      </p>
      <div className={classes["game-rules__player"]}>
        <p>
          <b>The player</b>
          <br />
          The player has to choose wheter to "hit" (ask for another card) or
          "stand" (don't ask for another card). Thus, the player may stand on
          the two cards originally dealt to them, or they may ask the dealer for
          additional cards, one at the time, until deciding to stand on the
          total (if it is 21 or under), or goes "bust" (if it is over 21).
        </p>
      </div>
      <div className={classes["game-rules__dealer"]}>
        <p>
          <b>The Dealer</b>
          <br />
          When it is the dealers turn, the dealers face-down card is turned up.
          If the total is 17 or more, the dealer must stand. If the total is
          under 16 the dealer must take a card. The dealer must continue to take
          cards until the total is 17 or more, at which point the dealer must
          stand. If the dealer has an ace, and counting it as 11 would bring the
          total to 17 or more (but not over 21), the dealer must count the ace
          as 11 and stand. The dealer's decisions, then, are automatic on all
          plays, whereas the player always has the option of taking one or more
          cards.
        </p>
      </div>
      <div className={classes["game-rules__values"]}>
        <ul>
          <b>Card values</b>
          <li>- An ace is worth 1 or 11</li>
          <li>- Face cards are worth 10</li>
          <li>- Any other card is its original value</li>
        </ul>
      </div>
    </div>
  );
};

export default GameRules;
